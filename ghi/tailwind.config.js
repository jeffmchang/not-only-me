/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontSize: {
        xss: "0.50rem",
      },
    },
  },
  plugins: [require("tailwindcss-animated")],
};
