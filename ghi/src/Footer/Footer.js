import React from "react";
import { Link } from "react-router-dom";
import { FaGithubSquare, FaInstagram, FaTwitterSquare } from "react-icons/fa";

const Footer = () => {
  return (
    <div className="bottom-0 w-full bg-[#40d7329f]">
      <div className="p-5 max-w-[1240px] mx-auto px-6 gap-8 grid lg:grid-cols-4">
        <div className="col-span-4 lg:col-span-1">
          <div className="w-full text-3xl font-bold text-white ">
            <h1>NOT ONLY ME.</h1>
          </div>
          <p className="text-gray-700 text-sm py-4">
            Stay connected with the latest health and wellness news,
            sustainability tips, and community resources by signing up for our
            updates. Our updates provide regular content to inspire and inform
            you on your journey towards a healthier, more sustainable lifestyle.
            You'll also be the first to know about upcoming events and
            opportunities to connect with our community. In addition to our
            updates, you can also join our community on social media. Follow us
            on Instagram, Twitter, and Facebook to stay up-to-date on our latest
            posts and to connect with other individuals who share your passion
            for wellness and sustainability. Thank you for visiting our website
            and for your interest in living a healthier, more sustainable life.
            We look forward to staying connected!
          </p>
          <div className="flex justify-between md:w-[50%] my-2">
            <FaGithubSquare size={30} color="black" />
            <FaInstagram size={30} color="black" />
            <FaTwitterSquare size={30} color="black" />
          </div>
        </div>
        <div className="lg:col-span-3 flex justify-between mx-20">
          <div>
            <h6 className="text-black font-semibold py-2">Support</h6>
            <ul className="text-black">
              <li className="py-2 text-sm">
                <a
                  href="https://gitlab.com/robin_kim/not-only-me"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Gitlab Repository
                </a>
              </li>
              <li className="py-2 text-sm">Technologies</li>
              <li className="py-2 text-sm">Complaints</li>
              <li className="py-2 text-sm">Report</li>
            </ul>
          </div>
          <div>
            <h6 className="text-black font-semibold py-2">Community</h6>
            <ul className="text-black">
              <li className="py-2 text-sm">
                <Link to="/">Join Today!</Link>
              </li>
              <li className="py-2 text-sm">
                <Link to="/">Access Account</Link>
              </li>
              <li className="py-2 text-sm">
                <Link to="/">Forgot Username?</Link>
              </li>
              <li className="py-2 text-sm">
                <Link to="/">Forgot Passwords?</Link>
              </li>
              <li className="py-2 text-sm">
                <Link to="/">Report Post</Link>
              </li>
            </ul>
          </div>
          <div>
            <h6 className="text-black font-semibold py-2">Company</h6>
            <ul className="text-black">
              <li className="py-2 text-sm">About Us</li>
              <li className="py-2 text-sm">Contact Us</li>
              <li className="py-2 text-sm">Location</li>
              <li className="py-2 text-sm">The Team</li>
            </ul>
          </div>
        </div>
      </div>
      <footer className="w-full text-center bg-black text-white p-4">
        &copy; 2023 Not Only Me. All rights reserved.
      </footer>
    </div>
  );
};

export default Footer;
