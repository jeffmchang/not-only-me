import { BrowserRouter, Routes, Route, useLocation } from "react-router-dom";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import Mainpage from "./Components/MainPage.js";
import Home from "./Components/Home.js";
import Footer from "./Footer/Footer.js";
import Signup from "./Login/Signup.js";
import Login from "./Login/Login.js";
import Community from "./Community/Community.js";
import PostForm from "./Post/PostForm.js";
import React, { useEffect } from "react";
import AboutUs from "./Components/AboutUs.js";

function ScrollToTop() {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}


function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  // const [showTempDiv, setShowTempDiv] = useState(true);

  // useEffect(() => {
  //   setTimeout(() => {
  //     setShowTempDiv(false);
  //   }, 3000);
  // }, []);


  return (
    <BrowserRouter basename={basename}>
      <AuthProvider
        tokenUrl={`${process.env.REACT_APP_USER_SERVICE_API_HOST}/token`}
      >
        <ScrollToTop />
        <Routes>
          <Route path="/" element={<Mainpage />} />
          <Route path="/home" element={<Home />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/login" element={<Login />} />
          <Route path="/community" element={<Community />} />
          <Route path="/post" element={<PostForm />} />
          <Route path="/about" element={<AboutUs />} />
        </Routes>
        <Footer />
      </AuthProvider>
    </BrowserRouter>
  );
}
export default App;
