import LoggedNavbar from "../Navbar/Navbar";
import { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { AddIcon } from "@chakra-ui/icons";
import { useNavigate } from "react-router-dom";

export default function PostForm() {
  const navigate = useNavigate();
  const { token } = useToken();
  const [user_id, setUserId] = useState("");
  const [topic, setTopic] = useState("");
  const [description, setDescription] = useState("");
  const currentDate = new Date().toISOString().slice(0, 10);
  const [posted_on, setPostedOn] = useState(currentDate);

  const handleTopicChange = (event) => setTopic(event.target.value);
  const handleDescriptionChange = (event) => setDescription(event.target.value);

  const fetchUser = async () => {
    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/token`;
    const response = await fetch(url, {
      method: "GET",
      credentials: "include",
    });
    if (response.ok) {
      const data = await response.json();
      setUserId(data.account);
    }
  };
  useEffect(() => {
    fetchUser();
  }, [token]);

  const handlePostSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.user_id = user_id.id;
    data.topic = topic;
    data.description = description;
    data.posted_on = posted_on;

    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/posts`;
    const fetchConfig = {
      method: "POST",
      credentials: "include",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setTopic("");
      setDescription("");
      setPostedOn("");
      navigate("/community")
    }
  };

  return (
    <>
      <LoggedNavbar />
      <div className="bg-[#c8ffd0] min-h-screen">
        <div className="min-h-screen flex justify-center items-center mx-5">
          <div className="bg-white shadow-md shadow-gray-400 px-8 py-10 mb-4 max-w-3xl">
            <h1 className="text-center text-4xl font-bold mb-10">
              GENERATE NEW COMMUNITY CONTENT
            </h1>
            <form onSubmit={handlePostSubmit} id="new-post">
              <div className="mb-5">
                <label className="font-bold">TOPIC:</label>
                <input
                  className="border rounded w-full py-2 px-3 pr-10 mt-2"
                  onChange={handleTopicChange}
                  required
                  type="text"
                  name="topic"
                  value={topic}
                />
              </div>
              <div className="mb-5">
                <label className="font-bold">DESCRIPTION:</label>
                <textarea
                  className="border rounded w-full pt-2 pb-5 px-3 pr-10 mt-2"
                  onChange={handleDescriptionChange}
                  required
                  type="text"
                  value={description}
                />
              </div>
              <button
                className="float-right mt-4 mx-2 rounded-full bg-blue-500 text-white p-2 px-3 hover:-translate-y-1 hover:scale-110 hover:bg-blue-600"
                type="submit"
              >
                <AddIcon />
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
