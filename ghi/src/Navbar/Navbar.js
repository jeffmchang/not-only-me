import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { AiOutlineMenu, AiOutlineClose } from "react-icons/ai";

export default function LoggedNavbar () {
  const navigate = useNavigate();
  const { logout } = useToken();
  const [nav, setNav] = useState(false);

  const handleNav = () => {
    setNav(!nav);
  };

  const handleLogout = (e) => {
    e.preventDefault();
    logout();
    handleNav();
    navigate("/");
  };

  return (
    <nav className="sticky top-0 z-10 bg-[#40d7329f] p-2 backdrop-filter backdrop-blur-lg">
      <div className="flex justify-between items-center h-20 max-w-[1240px] mx-auto px-4 text-white">
        <h1 className="animate-bounce animate-once">
          <span className="">
            <Link
              to="/home"
              className="w-full text-5xl font-bold text-white hover:text-black"
            >
              NOM.
            </Link>
          </span>
        </h1>
        <ul className="md:flex hidden">
          <li className="p-4 font-bold text-xl hover:text-black">
            <Link to="/home">Home</Link>
          </li>
          <li className="p-4 font-bold text-xl hover:text-black">
            <Link to="/about">About Us</Link>
          </li>
          <li className="p-4 font-bold text-xl hover:text-black">
            <Link to="/community">Community</Link>
          </li>
          <li className="p-4 font-bold text-xl hover:text-black">
            <button onClick={handleLogout}>Log Out</button>
          </li>
        </ul>
        <div
          onClick={handleNav}
          className="md:hidden pt-2 flex justify-end pr-1"
        >
          <AiOutlineMenu size={30} />
        </div>
        <div
          className={
            nav
              ? "fixed top-0 right-0 w-[40%] h-full bg-[#000] ease-in-out duration-500 transition-all"
              : "hidden"
          }
        >
          <div onClick={handleNav} className="flex justify-end pr-4 pt-7">
            <AiOutlineClose size={30} />
          </div>
          <ul className="bg-[#000] h-screen pl-3">
            <li className="p-2 font-bold text-xl hover:text-green-400">
              <Link to="/home" onClick={handleNav}>
                Home
              </Link>
            </li>
            <li className="p-2 font-bold text-xl hover:text-green-400">
              <Link to="/about" onClick={handleNav}>
                About Us
              </Link>
            </li>
            <li className="p-2 font-bold text-xl hover:text-green-400">
              <Link to="/community" onClick={handleNav}>
                Community
              </Link>
            </li>
            <li className="p-2 font-bold text-xl hover:text-green-400">
              <button onClick={handleLogout}>Log Out</button>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};
