import LoggedNavbar from "../Navbar/Navbar";
import Ocean from "../Assets/ocean-circle.jpg";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState, useEffect } from "react";
import { SmallAddIcon } from "@chakra-ui/icons";
import { Link } from "react-router-dom";

export default function Community() {
  const { token } = useToken();
  const [user, setUser] = useState("");
  const [users, setUsers] = useState([]);
  const [posts, setPosts] = useState([]);
  const [user_id, setUserId] = useState("");
  const [post_id, setPostId] = useState([]);
  const [comment, setComment] = useState("");
  const [comments, setComments] = useState([]);
  const [postcomments, setPostComments] = useState([]);

  const handleCommentChange = (event) => setComment(event.target.value);

  const fetchUser = async () => {
    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/token`;
    const response = await fetch(url, {
      method: "GET",
      credentials: "include",
    });
    if (response.ok) {
      const data = await response.json();
      setUser(data.account);
      setUserId(data.account);
    }
  };
  useEffect(() => {
    fetchUser();
  }, [token]);

  const fetchAllUsers = async () => {
    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/users`;
    const response = await fetch(url, {
      method: "GET",
      credentials: "include",
    });
    if (response.ok) {
      const data = await response.json();
      setUsers(data);
    }
  };
  useEffect(() => {
    fetchAllUsers();
  }, []);

  const fetchPost = async () => {
    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/posts/${post_id}`;
    const response = await fetch(url, {
      method: "GET",
      credentials: "include",
    });
    if (response.ok) {
      const data = await response.json();
      setPostId(data);
    }
  };
  useEffect(() => {
    fetchPost();
  }, [post_id]);

  const fetchAllPosts = async () => {
    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/posts`;
    const response = await fetch(url, {
      method: "GET",
      credentials: "include",
    });
    if (response.ok) {
      const data = await response.json();
      setPosts(data);
    }
  };
  useEffect(() => {
    fetchAllPosts();
  }, []);

  const fetchPostComments = async () => {
    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/posts/${post_id}/comments`;
    const response = await fetch(url, {
      method: "GET",
      credentials: "include",
    });
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setComments(data);
    }
  };
  useEffect(() => {
    fetchPostComments();
  }, [post_id]);

  const fetchAllComments = async () => {
    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/comments`;
    const response = await fetch(url, {
      method: "GET",
      credentials: "include",
    });
    if (response.ok) {
      const data = await response.json();
      setPostComments(data);
    }
  };
  useEffect(() => {
    fetchAllComments();
  }, []);

  const handleCommentSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.user_id = user_id.id;
    data.post_id = post_id.id;
    data.comment = comment;

    const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/comments`;
    const fetchConfig = {
      method: "POST",
      credentials: "include",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setComment("");
      fetchAllComments();
    }
  };

  return (
    <>
      <LoggedNavbar />
      <div
        className="bg-cover bg-no-repeat min-h-screen"
        style={{
          backgroundImage: `url(${Ocean})`,
          backgroundAttachment: "fixed",
        }}
      >
        <div className="flex flex-col w-full bg-cover mx-auto my-auto">
          <h1 className="bg-[#C4DDFF] bg-opacity-80 justify-center flex p-64 text-7xl text-white font-bold">
            COMMUNITY
          </h1>
          <div className="flex-1 flex flex-grow">
            <div className="w-2/5 bg-white flex flex-col items-center">
              <div className="text-white bg-[#22577E] py-6 font-bold w-full text-center">
                <h1 className="mx-auto my-6 lg:text-3xl md:text-2xl sm:text-xl text-lg">
                  TRENDING TOPICS
                </h1>
                <ol className="text-left mx-8">
                  <li>1: </li>
                  <li>2: </li>
                  <li>3: </li>
                </ol>
              </div>
              <div className="w-full p-32 bg-black text-center font-semibold text-white mx-auto">
                <h2 className="text-3xl">DIV SECTION</h2>
              </div>
              <div className="w-full p-32 bg-gray-900 text-center font-semibold text-white mx-auto">
                <h2 className="text-3xl">DIV SECTION</h2>
              </div>
              <div className="w-full p-32 bg-black text-center font-semibold text-white mx-auto">
                <h2 className="text-3xl">DIV SECTION</h2>
              </div>
              <div className="w-full p-32 bg-gray-900 text-center font-semibold text-white mx-auto">
                <h2 className="text-3xl">DIV SECTION</h2>
              </div>
            </div>
            <div className="w-full bg-gray-100 mx-auto flex-col flex items-center justify-center">
              <div className="w-4/5 py-16">
                {posts.map((post) => {
                  return (
                    <div
                      className="bg-shadow-100 bg-gradient-to-b from-white shadow-lg m-6 p-5"
                      key={post.id}
                    >
                      <div className="font-semibold mb-2 flex">
                        <div className="flex-grow opacity-50" key={user}>
                          {users
                            .find((user) => user.id === post.user_id)
                            ?.username.slice(0, 1)
                            .toUpperCase() +
                            users
                              .find((user) => user.id === post.user_id)
                              ?.username.slice(1)}
                        </div>
                        <div className="text-sm text-gray-500 opacity-50">
                          {(() => {
                            const postedOn = new Date(post.posted_on);
                            const now = new Date();
                            const diff = now - postedOn;
                            const diffInHours = Math.round(
                              diff / (1000 * 60 * 60)
                            );
                            const diffInDays = Math.round(
                              diff / (1000 * 60 * 60 * 24)
                            );
                            if (diffInHours < 24) {
                              return `${diffInHours} hour${
                                diffInHours > 1 ? "s" : ""
                              } ago`;
                            } else {
                              return `${diffInDays} day${
                                diffInDays > 1 ? "s" : ""
                              } ago`;
                            }
                          })()}
                        </div>
                      </div>
                      <div className="mb-5 text-xl font-bold flex-grow">
                        {post.topic}
                      </div>
                      <div className="text-xl font-bold mt-2 flex">
                        <div className="break-normal font-normal">
                          {post.description}
                        </div>
                      </div>
                      <button
                        className="mt-8"
                        type="button"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M20.893 13.393l-1.135-1.135a2.252 2.252 0 01-.421-.585l-1.08-2.16a.414.414 0 00-.663-.107.827.827 0 01-.812.21l-1.273-.363a.89.89 0 00-.738 1.595l.587.39c.59.395.674 1.23.172 1.732l-.2.2c-.212.212-.33.498-.33.796v.41c0 .409-.11.809-.32 1.158l-1.315 2.191a2.11 2.11 0 01-1.81 1.025 1.055 1.055 0 01-1.055-1.055v-1.172c0-.92-.56-1.747-1.414-2.089l-.655-.261a2.25 2.25 0 01-1.383-2.46l.007-.042a2.25 2.25 0 01.29-.787l.09-.15a2.25 2.25 0 012.37-1.048l1.178.236a1.125 1.125 0 001.302-.795l.208-.73a1.125 1.125 0 00-.578-1.315l-.665-.332-.091.091a2.25 2.25 0 01-1.591.659h-.18c-.249 0-.487.1-.662.274a.931.931 0 01-1.458-1.137l1.411-2.353a2.25 2.25 0 00.286-.76m11.928 9.869A9 9 0 008.965 3.525m11.928 9.868A9 9 0 118.965 3.525"
                          />
                        </svg>
                      </button>
                      <div className="font-bold bg-white mt-2 p-5 w-full">
                        {postcomments.map((comment) => {
                          return (
                            <table
                              className="text-md font-normal w-full shadow-md shadow-black-400 my-2"
                              key={comment.id}
                            >
                              <tbody>
                                <tr>
                                  <td className="break-normal text-md p-2">
                                    {comment.comment}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          );
                        })}
                      </div>
                      <div className="font-bold bg-white mt-6 p-5 w-full">
                        {comments.map((comment) => {
                          return (
                            <table
                              className="text-md font-normal w-full shadow-md shadow-black-400 my-2"
                              key={comment.id}
                            >
                              <tbody>
                                <tr>
                                  <td className="break-normal text-md p-2">
                                    {comment.comment}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          );
                        })}
                      </div>
                      <form
                        onSubmit={handleCommentSubmit}
                        id="new-comment"
                        className="bg-white font-normal flex items-center mt-4"
                      >
                        <input
                          className="w-full p-2"
                          onChange={handleCommentChange}
                          placeholder="&nbsp;New comment"
                          required
                          type="text"
                          name="comment"
                          value={comment}
                          id="comment"
                        />
                        <button className="px-2" type="submit">
                          <SmallAddIcon />
                        </button>
                      </form>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
        <div className="bg-blue-400 bg-opacity-40 text-center py-32 flex justify-center display items-center">
          <div className="shadow-2xl py-16 w-1/2">
            <h2 className="font-bold text-white lg:text-4xl md:text-3xl sm:text-2xl text-xl py-8 mx-auto">
              SHARE, EDUCATE, AND GUIDE
            </h2>
            <Link
              to="/post"
              className="text-gray-900 bg-green-400 text-sm sm:text-md md:text-lg lg:text-xl font-bold p-4 rounded-2xl hover:bg-green-500"
            >
              SHARE
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
