import React from "react";
import Garden from "../Assets/garden.jpg";
import LoggedNavbar from "../Navbar/Navbar";
import { Link } from "react-router-dom";
import { AddIcon, ArrowDownIcon } from "@chakra-ui/icons";

function Mainpage() {
  return (
    <>
      <LoggedNavbar />
      <div
        className="bg-cover bg-no-repeat"
        style={{
          backgroundImage: `url(${Garden})`,
          backgroundAttachment: "fixed",
        }}
      >
        <div className="bg-black bg-opacity-80 py-32 flex justify-center items-center">
          <h2 className="animate-fade w-[500px] h-[500px] text-white bg-[#63c468] font-bold lg:text-4xl md:text-3xl sm:text-3xl text-3xl text-center break-words px-6 flex items-center justify-center">
            BECOME THE BRIDGE TO SPREAD AWARENESS
          </h2>
        </div>
        <div className="bg-white grid grid-cols-2 gap-10 py-24 lg:px-44 md:px-20 sm:px-12 px-4">
          <div className="bg-[#f9f6ce] shadow-lg p-4 text-center rounded-3xl py-16 relative">
            <h2 className="text-green-500 lg:text-2xl text-lg font-bold mb-6">
              Find Expert Advice
            </h2>
            <p className="font-semibold text-gray-600 px-10 py-4">
              Find a wealth of information and resources to help you lead a
              happier, healthier life. Our team of experts includes doctors,
              nutritionists, and other healthcare professionals who are
              dedicated to helping you achieve your wellness goals. So why wait?
              Start exploring our site today and take the first step towards a
              healthier you!
            </p>
            <Link
              to="/experts"
              className="text-green-500 font-semibold mt-4 hover:text-green-600 absolute bottom-6 left-0 right-0"
            >
              Find an Expert
            </Link>
          </div>
          <div className="bg-[#E8F9FD] shadow-lg p-4 text-center rounded-3xl py-16 relative">
            <h2 className="text-green-500 lg:text-2xl text-lg font-bold mb-6">
              Share Personal Tips
            </h2>
            <p className="font-semibold text-gray-600 px-10 py-4">
              Looking for a way to share your personal tips for living a
              sustainable lifestyle? N.O.M is the perfect platform to connect
              with others and inspire change. Share your experiences, ideas, and
              advice on reducing waste, conserving energy, and choosing
              environmentally-friendly products. With a supportive community of
              like-minded individuals, you can make a positive impact and help
              create a more sustainable future for us all.
            </p>
            <Link
              to="/community"
              className="text-green-500 font-semibold mt-4 hover:text-green-600 absolute bottom-6 left-0 right-0"
            >
              Join our Community
            </Link>
          </div>
          <div className="bg-[#E8F9FD] shadow-lg p-4 text-center rounded-3xl py-16 relative">
            <h2 className="text-green-500 lg:text-2xl text-lg font-bold mb-6">
              Connect with Community Members
            </h2>
            <p className="font-semibold text-black text-center px-10 py-4">
              Connect with like-minded individuals who share your passion for
              health, wellness, and sustainability. Whether you're looking for
              advice on reducing waste, finding eco-friendly products, or
              improving your mental and physical well-being, our community
              members are here to support you. Join the conversation today and
              connect with a diverse community of individuals committed to
              living a healthier, more sustainable lifestyle.
            </p>
            <Link
              to="/"
              className="text-green-500 font-semibold mt-4 hover:text-green-600 absolute bottom-6 left-0 right-0"
            >
              Explore Sustainability
            </Link>
          </div>
          <div className="bg-[#f9f6ce] shadow-lg p-4 text-center rounded-3xl py-16 relative">
            <h2 className="text-green-500 lg:text-2xl text-lg font-bold mb-6">
              Sustainable Living Resources
            </h2>
            <p className="font-semibold text-gray-600 px-10 py-4">
              Living sustainably is important for the health of our planet and
              future generations. At N.O.M, we understand the value of making
              small changes in our daily lives to reduce our environmental
              impact. From tips on reducing waste and conserving energy, to
              choosing environmentally-friendly products and supporting ethical
              businesses, our website provides the tools you need to make
              positive changes.
            </p>
            <Link
              to="/"
              className="text-green-500 font-semibold mt-4 hover:text-green-600 absolute bottom-6 left-0 right-0"
            >
              Explore Sustainability
            </Link>
          </div>
        </div>
        <div className="bg-black text-white py-20 flex flex-col md:flex-row items-center">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-6 md:gap-12 items-center justify-center">
            <div className="font-bold text-2xl flex flex-col items-center justify-center md:border-r-4 md:border-white lg:border-r-4 lg:border-white">
              <div className="bg-blue-400 h-64 w-64 rounded-full flex items-center justify-center">
                You
              </div>
              <p className="text-center font-bold mt-6">
                <ArrowDownIcon />
              </p>
              <div className="bg-green-400 h-64 w-64 rounded-full flex items-center justify-center mt-6">
                Others
              </div>
              <p className="text-center font-bold mt-6">
                <ArrowDownIcon />
              </p>
              <div className="bg-teal-400 h-64 w-64 rounded-full flex items-center justify-center mt-6">
                Community
              </div>
            </div>
            <div className="text-center md:text-left lg:mr-32 md:mr-10 mx-2">
              <h2 className="text-4xl font-bold my-4">THE IMPACT</h2>
              <p className="text-lg pt-2">
                By sharing tips and advice on being green, recycling, and
                prioritizing our wellbeing, we can collectively have a
                tremendous impact on our communities and the planet. When we
                educate ourselves and share our knowledge with others, we
                inspire positive change and encourage others to take action.
                Whether it's small changes in our daily lives or larger
                initiatives, every effort we make towards a sustainable future
                is valuable. By prioritizing the health of our planet and
                ourselves, we can create a better world for everyone.
              </p>
            </div>
          </div>
        </div>
        <div className="bg-white py-8 md:py-16 lg:py-32">
          <h2 className="text-center font-bold pb-10 text-4xl">
            CHECK OUT OTHER CONTENT
          </h2>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 lg:px-32 md:px-16 sm:px-8 px-8 justify-items-center">
            <div className="w-full sm:w-[250px] h-[250px] text-gray-100 bg-green-400 bg-gradient-to-b from-green-300 rounded-2xl transform motion-safe:hover:scale-110 p-6 flex flex-col justify-center items-center py-16">
              <h2 className="text-2xl font-bold mb-6 text-center">LEARN</h2>
              <Link
                to="/home"
                className="shadow-md bg-blue-500 text-white font-bold flex flex-col justify-center items-center w-9 h-9 rounded-full hover:-translate-y-1 hover:scale-110 hover:bg-blue-600 transition delay-100 duration-300 ease-in-out"
              >
                <AddIcon />
              </Link>
            </div>
            <div className="w-full sm:w-[250px] h-[250px] text-gray-100 bg-green-400 bg-gradient-to-b from-green-300 rounded-2xl transform motion-safe:hover:scale-110 p-6 flex flex-col justify-center items-center py-16">
              <h2 className="text-2xl font-bold mb-6 text-center">SHARE</h2>
              <Link
                to="/home"
                className="shadow-md bg-blue-500 text-white font-bold flex flex-col justify-center items-center w-9 h-9 rounded-full hover:-translate-y-1 hover:scale-110 hover:bg-blue-600 transition delay-100 duration-300 ease-in-out"
              >
                <AddIcon />
              </Link>
            </div>
            <div className="w-full sm:w-[250px] h-[250px] text-gray-100 bg-green-400 bg-gradient-to-b from-green-300 rounded-2xl transform motion-safe:hover:scale-110 p-6 flex flex-col justify-center items-center py-16">
              <h2 className="text-2xl font-bold mb-6 text-center">POST</h2>
              <Link
                to="/post"
                className="shadow-md bg-blue-500 text-white font-bold flex flex-col justify-center items-center w-9 h-9 rounded-full hover:-translate-y-1 hover:scale-110 hover:bg-blue-600 transition delay-100 duration-300 ease-in-out"
              >
                <AddIcon />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Mainpage;
