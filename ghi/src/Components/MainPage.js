import React from "react";
import Ferns from "../Assets/ferns.jpg";
import WorldHand from "../Assets/world-hand.jpg";
import Typed from "react-typed";
import LoggedOutNavbar from "../Navbar/LoggedOutNav";

function Mainpage() {
  return (
    <>
      <LoggedOutNavbar />
      <div
        className="bg-cover bg-no-repeat min-h-screen"
        style={{
          backgroundImage: `url(${Ferns})`,
          backgroundAttachment: "fixed",
        }}
      >
        <h1 className="lg:text-5xl md:text-4xl sm:text-3xl text-3xl text-center font-bold text-white bg-black bg-opacity-80 w-full py-64">
          WELCOME TO{" "}
          <Typed
            className="lg:text-5xl md:text-4xl sm:text-3xl text-3xl font-bold text-green-400"
            strings={["NOT ONLY ME", "N.O.M"]}
            style={{ textDecoration: "underline" }}
            typeSpeed={110}
            backSpeed={110}
            loop
            loopDelay={6000}
          />
        </h1>
        <div className="py-40 bg-white bg-gradient-to-b from-teal-100 text-center"></div>
        <div className="flex flex-col items-center bg-white p-10 md:p-16">
          <div className="w-96 h-96 rounded-full shadow-lg shadow-sky-100 overflow-hidden mb-4 animate-fade animate-once">
            <img
              src={WorldHand}
              alt=""
              className="w-full h-full object-cover"
            />
          </div>
          <div className="flex justify-center">
            <p className="text-sky-500 text-2xl font-semibold text-center lg:max-w-4xl md:max-w-3xl sm:max-w-2xl max-w-md px-10">
              Your go-to destination for health and wellness resources, expert
              advice, and community support. We also offer tips and resources
              for living a sustainable lifestyle, so you can make positive
              changes that benefit both your health and the environment. Let's
              join hands in building a healthier, happier, and more sustainable
              world together!
            </p>
          </div>
        </div>
        <div className="bg-[#a37c21] bg-gradient-to-b from-white py-40 text-center">
        </div>
      </div>
    </>
  );
}

export default Mainpage;
