import LoggedNavbar from "../Navbar/Navbar";
import React from "react";
import Moon from "../Assets/moon.jpg";
import Greenland from "../Assets/green-land.jpg";
import TreeSphere2 from "../Assets/tree-sphere-2.jpg";

export default function AboutUs() {
  return (
    <>
      <LoggedNavbar />
      <div
        className="bg-cover bg-no-repeat min-h-screen"
        style={{
          backgroundImage: `url(${Moon})`,
          backgroundAttachment: "fixed",
        }}
      >
        <h1 className="lg:text-7xl md:text-6xl sm:text-5xl text-4xl text-center font-bold text-white w-full py-64">
          ABOUT US
        </h1>
        <div className="w-full bg-[#F1F6F9] px-32 py-40">
          <div className="w-full animate-fade-right animate-once bg-[#212A3E] md:w-3/5 mx-auto flex flex-col md:flex-row md:justify-between shadow-lg p-4 md:p-5 shadow-gray-200">
            <img
              className="rounded-full md:w-64 h-64 md:mr-10 order-2 md:order-1 shadow-gray-300 shadow-md"
              src={Greenland}
              alt=""
            />
            <div className="w-full md:w-3/4 p-4 md:p-5 order-1 md:order-2">
              <h2 className="text-white text-2xl font-bold text-center mb-3">
                Mission Statement
              </h2>
              <p className="text-gray-100 text-center">
                Lorem ipsum dolor pretium nunc sit amet, consectetur adipiscing
                elit. Mauris porta mi sed neque ultrices facilisis. In pretium
                nunc eu mauris pretium, non volutpat neque suscipit. Lorem ipsum
                dolor pretium nunc sit amet, consectetur adipiscing elit. Mauris
                porta mi sed neque ultrices facilisis.
              </p>
            </div>
          </div>
          <div className="w-full animate-fade-left animate-once bg-[#394867] md:w-3/5 mx-auto flex flex-col md:flex-row md:justify-between shadow-lg p-4 md:p-5 shadow-gray-200">
            <img
              className="rounded-full md:w-64 h-64 md:ml-10 order-2 md:order-2 shadow-gray-300 shadow-md"
              src={TreeSphere2}
              alt=""
            />
            <div className="w-full md:w-3/4 p-4 md:p-5 order-1">
              <h2 className="text-white text-2xl font-bold text-center mb-3">
                Company Motto
              </h2>
              <p className="text-gray-100 text-center">
                Lorem ipsum dolor pretium nunc sit amet, consectetur adipiscing
                elit. Mauris porta mi sed neque ultrices facilisis. In pretium
                nunc eu mauris pretium, non volutpat neque suscipit. Lorem ipsum
                dolor pretium nunc sit amet, consectetur adipiscing elit. Mauris
                porta mi sed neque ultrices facilisis.
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
