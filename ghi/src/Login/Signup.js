import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import LoggedOutNavbar from "../Navbar/LoggedOutNav";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";

export default function Signup() {
  const navigate = useNavigate();
  const [first, setFirst] = useState("");
  const [last, setLast] = useState("");
  const [username, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const handleFirstChange = (e) => {
    setFirst(e.target.value);
  };
  const handleLastChange = (e) => {
    setLast(e.target.value);
  };
  const handleUserNameChange = (e) => {
    setUserName(e.target.value);
  };
  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };
  const toggleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first = first;
    data.last = last;
    data.username = username;
    data.email = email;
    data.password = password;

    const userUrl = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/users`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(userUrl, fetchConfig);
    if (response.ok) {
      setFirst("");
      setLast("");
      setUserName("");
      setEmail("");
      setPassword("");
      navigate("/login");
    }
  };

  return (
    <>
      <LoggedOutNavbar />
      <div className="bg-[#c8ffd0] min-h-screen">
        <div className="min-h-screen flex justify-center items-center">
          <div className="bg-white shadow-md shadow-gray-400 px-8 py-10 mb-4 max-w-2xl w-full">
            <h1 className="text-center text-4xl font-bold mb-10">Sign Up</h1>
            <form onSubmit={handleSubmit} id="new-user">
              <div className="flex flex-row mb-5">
                <div className="w-1/2 pr-2">
                  <label
                    className="text-lg block font-bold mb-2"
                    htmlFor="first"
                  >
                    First Name&nbsp;
                    <span className="text-red-500 font-normal">*</span>
                  </label>
                  <input
                    className="border rounded w-full py-2 px-3"
                    onChange={handleFirstChange}
                    required
                    type="text"
                    name="first"
                    value={first}
                  />
                </div>
                <div className="w-1/2 pl-2">
                  <label
                    className="text-lg block font-bold mb-2"
                    htmlFor="last"
                  >
                    Last Name
                  </label>
                  <input
                    className="border rounded w-full py-2 px-3"
                    onChange={handleLastChange}
                    required
                    type="text"
                    name="last"
                    value={last}
                  />
                </div>
              </div>
              <div className="mb-5">
                <label
                  className="text-lg block font-bold mb-2"
                  htmlFor="username"
                >
                  Username&nbsp;
                  <span className="text-red-500 font-normal">*</span>
                </label>
                <input
                  className="border rounded w-full py-2 px-3"
                  onChange={handleUserNameChange}
                  required
                  type="text"
                  name="username"
                  value={username}
                />
              </div>
              <div className="mb-5">
                <label className="text-lg block font-bold mb-2" htmlFor="email">
                  Email Address&nbsp;
                  <span className="text-red-500 font-normal">*</span>
                </label>
                <input
                  className="border rounded w-full py-2 px-3"
                  onChange={handleEmailChange}
                  required
                  type="text"
                  name="email"
                  value={email}
                />
              </div>
              <div className="mb-8">
                <label
                  className="text-lg block font-bold mb-2"
                  htmlFor="password"
                >
                  Password&nbsp;
                  <span className="text-red-500 font-normal">*</span>
                </label>
                <div className="relative">
                  <input
                    className="border rounded w-full py-2 px-3 pr-10"
                    onChange={handlePasswordChange}
                    required
                    type={showPassword ? "text" : "password"}
                    name="password"
                    value={password}
                  />
                  <button
                    className="absolute right-2 top-0 h-full w-10 text-gray-400 hover:text-gray-500 flex items-center justify-center"
                    onClick={toggleShowPassword}
                    type="button"
                  >
                  {showPassword ? <ViewIcon /> : <ViewOffIcon />}
                  </button>
                </div>
              </div>
              <button
                className="w-full bg-blue-500 hover:bg-blue-600 text-white font-bold py-3 rounded-xl mb-2"
                type="submit"
              >
                Sign Up
              </button>
              <p className="text-md font-semibold pt-2">
                Already a user?
                <Link to="/login" className="pl-2 text-blue-500 font-semi-bold">
                  Login
                </Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
