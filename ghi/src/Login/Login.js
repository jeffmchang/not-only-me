import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { useToast } from "@chakra-ui/react";
import LoggedOutNavbar from "../Navbar/LoggedOutNav";

export default function Signup() {
    const toast = useToast();
    const navigate = useNavigate();
    const { login, token } = useToken();
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [showPassword, setShowPassword] = useState(false);

    const handleUserNameChange = (e) => {
        setUserName(e.target.value);
    };
    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };
    const toggleShowPassword = () => {
    setShowPassword(!showPassword);
    };

    const handleSubmit = async (event) => {
      event.preventDefault();
      try {
        await login(username, password);
      } catch (error) {
        toast({
          title: "An error occurred.",
          description: "Incorrect Username or Password.",
          status: "error",
          duration: 3000,
          isClosable: true,
        });
      }
    };

    useEffect(() => {
    if (token) {
        navigate("/home");
    }
    }, [token, navigate]);


  return (
    <>
      <LoggedOutNavbar/>
      <div className="bg-[#c8ffd0] min-h-screen">
        <div className="min-h-screen flex justify-center items-center">
          <div className="bg-white shadow-md shadow-gray-400 px-8 py-10 mb-4 max-w-xl w-full">
            <h1 className="text-center text-4xl font-bold mb-10">Login</h1>
            <form onSubmit={handleSubmit} id="new-user">
              <div className="mb-5">
                <label
                  className="text-lg block font-bold mb-2"
                  htmlFor="username"
                >
                  Username&nbsp;
                  <span className="text-red-500 font-normal">*</span>
                </label>
                <input
                  className="border rounded w-full py-2 px-3"
                  onChange={handleUserNameChange}
                  required
                  type="text"
                  name="name"
                  value={username}
                />
              </div>
              <div className="mb-8">
                <label
                  className="text-lg block font-bold mb-2"
                  htmlFor="password"
                >
                  Password&nbsp;
                  <span className="text-red-500 font-normal">*</span>
                </label>
                <div className="relative">
                  <input
                    className="border rounded w-full py-2 px-3 pr-10"
                    onChange={handlePasswordChange}
                    required
                    type={showPassword ? "text" : "password"}
                    name="password"
                    value={password}
                  />
                  <button
                    className="absolute right-2 top-0 h-full w-10 text-gray-400 hover:text-gray-500 flex items-center justify-center"
                    onClick={toggleShowPassword}
                    type="button"
                  >
                    {showPassword ? <ViewIcon /> : <ViewOffIcon /> }
                  </button>
                </div>
              </div>
              <button
                className="w-full bg-blue-500 hover:bg-blue-600 text-white font-bold py-3 rounded-xl mb-2"
                type="submit"
              >
                Login
              </button>
              <p className="text-md font-semibold pt-2">
                Not registered?
                <Link
                  to="/signup"
                  className="pl-2 text-blue-500 font-semi-bold"
                >
                  Sign up
                </Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
