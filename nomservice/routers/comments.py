from fastapi import APIRouter, Depends, Response, Request
from typing import List, Optional, Union
from authenticator import authenticator
from jwtdown_fastapi.authentication import Token
from queries.comments import Error, CommentIn, CommentOut, CommentRepository
from queries.users import UserOut


class AccountToken(Token):
    account: UserOut


router = APIRouter()


@router.get("/api/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    print(account_data)
    return True


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": user,
        }


@router.post("/api/comments", response_model=Union[CommentOut, Error])
def create_comment(
    comment: CommentIn,
    response: Response,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    response = repo.create_comment(comment)
    if not response:
        response.status_code = 400
    else:
        return response


@router.delete("/api/comments/{comment_id}", response_model=bool)
def delete_comment(
    comment_id: int,
    response: Response,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    response = repo.delete_comment(comment_id)
    if not response:
        response.status_code = 400
    return response


@router.put("/api/comments/{comment_id}", response_model=Union[CommentOut, Error])
def update_comment(
    comment_id: int,
    comment: CommentIn,
    response: Response,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[CommentOut, Error]:
    response = repo.update_comment(comment_id, comment)
    if not response:
        response.status_code = 400
    return response


@router.get("/api/comments", response_model=Union[List[CommentOut], Error])
def get_all_comments(
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all_comments()


@router.get("/api/comments/{comment_id}", response_model=Optional[CommentOut])
def get_one_comment(
    comment_id: int,
    response: Response,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> CommentOut:
    comment = repo.get_one_comment(comment_id)
    if not comment:
        response.status_code = 400
    return comment


@router.get("/api/posts/{post_id}/comments", response_model=Union[List[CommentOut], Error])
def get_all_posts_comments(
    post_id: int,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.get_all_post_comments(post_id)
