from pydantic import BaseModel
from typing import List, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class CommentIn(BaseModel):
    user_id: int
    post_id: int
    comment: str


class CommentOut(BaseModel):
    id: int
    user_id: int
    post_id: int
    comment: str


class CommentRepository:
    def create_comment(self, comment: CommentIn) -> Union[CommentIn, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO comments (user_id, post_id, comment)
                        VALUES (
                            (SELECT id FROM users WHERE id = %s),
                            (SELECT id FROM posts WHERE id = %s),
                            %s
                        )
                        RETURNING id;
                        """,
                        [comment.user_id, comment.post_id, comment.comment],
                    )
                    id = result.fetchone()[0]
                    if id is None:
                        return None
                    return self.record_to_comment_in_to_out(id, comment)
        except Exception:
            return None

    def get_all_comments(self) -> Union[List[CommentOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            user_id,
                            post_id,
                            comment
                        FROM comments
                        ORDER BY id
                        """
                    )
                    return [
                        self.record_to_comment_out(record) for record in result
                    ]
        except Exception:
            return None

    def get_one_comment(self, comment_id: int) -> Union[CommentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            user_id,
                            post_id,
                            comment
                        FROM comments
                        WHERE id = %s
                        """,
                        [comment_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_comment_out(record)
        except Exception:
            return {"message": "Comment not found"}

    def get_all_user_comments(self, comment_id: int) -> Union[List[CommentOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            user_id,
                            post_id,
                            comment
                        FROM comments
                        WHERE user_id = %s
                        ORDER BY id
                        """,
                        [comment_id],
                    )
                    return [
                        self.record_to_comment_out(record) for record in result
                    ]
        except Exception:
            return None

    def get_all_post_comments(self, post_id: int) -> Union[List[CommentOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, user_id, post_id, comment
                        FROM comments
                        WHERE post_id = %s
                        """,
                        [post_id],
                    )
                    return [self.record_to_comment_out(record) for record in result]
        except Exception:
            return None

    def update_comment(self, comment_id: int, comment: CommentIn) -> Union[CommentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM comments
                        WHERE id = %s
                        """,
                        [comment_id],
                    )
                    fetch = result.fetchone()
                    if not fetch:
                        return {"message": "Comment does not exist"}
                    db.execute(
                        """
                        UPDATE comments
                        SET user_id = %s,
                            post_id = %s,
                            comment = %s
                        WHERE id = %s
                        """,
                        [
                            comment.user_id,
                            comment.post_id,
                            comment.comment,
                            comment_id
                        ],
                    )
                    return self.record_to_comment_in_to_out(comment_id, comment)
        except Exception:
            return {"message": "Updating a comment failed"}

    def delete_comment(self, comment_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as connection:
                with connection.cursor() as db:
                    db.execute(
                        """
                        DELETE from comments
                        WHERE id = %s
                        """,
                        [comment_id],
                    )
                    return True
        except Exception:
            return {"message": "Comment does not exist"}

    def record_to_comment_in_to_out(self, id: int, post: CommentIn):
        old_data = post.dict()
        return CommentOut(id=id, **old_data)

    def record_to_comment_out(self, record):
        return CommentOut(
            id=record[0],
            user_id=record[1],
            post_id=record[2],
            comment=record[3],
        )
