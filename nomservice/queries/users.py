from pydantic import BaseModel
from typing import Optional, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class UserIn(BaseModel):
    first: Optional[str]
    last: Optional[str]
    username: Optional[str]
    email: Optional[str]
    password: Optional[str]


class UserOut(BaseModel):
    id: int
    first: str
    last: str
    username: str
    email: str


class DuplicateUserError(ValueError):
    pass


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserRepository:
    def create_user(
        self, user: UserIn, hashed_password: str
    ) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users (first, last, username, email, hashed_password)
                        VALUES (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [user.first, user.last, user.username, user.email, hashed_password],
                    )
                    id = result.fetchone()[0]
                    if id is None:
                        return None
                    return self.record_to_user_in_to_out(
                        id, user, hashed_password
                    )
        except Exception as e:
            print("Create did not work", e)
            return None

    def get_one_user(self, username: str) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , first
                            , last
                            , username
                            , email
                            , hashed_password
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_hashed_user_out(record)
        except Exception:
            return {"message": "User not found"}

    def get_all_users(self) -> Union[UserOutWithPassword, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, first, last, username, email
                        FROM users
                        ORDER BY id
                        """
                    )
                    return [
                        self.record_to_user_out(record) for record in result
                    ]
        except Exception:
            return None


    def update_user(self, user_id: int, user: UserIn) -> Union[UserOut, Error]:
        try:
            with pool.connection() as connection:
                with connection.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM users
                        WHERE id = %s
                        """,
                        [user_id],
                    )
                    fetch = result.fetchone()
                    if not fetch:
                        return {"message": "User does not exist"}
                    db.execute(
                        """
                        UPDATE users
                        SET first = %s, last = %s, username = %s, email = %s
                        WHERE id = %s
                        """,
                        [user.first, user.last, user.username, user.email, user_id],
                    )
                    return self.record_to_user_in_to_out_without_password(
                        user_id, user
                    )
        except Exception:
            return {"message": "User could not be updated"}

    def delete_user(self, user_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as connection:
                with connection.cursor() as db:
                    db.execute(
                        """
                        DELETE from users
                        WHERE id = %s
                        """,
                        [user_id],
                    )
                    return True
        except Exception:
            return {"message": "User does not exist"}

    def record_to_user_in_to_out(
        self, id: int, user: UserIn, hashed_password: str
    ):
        old_data = user.dict()
        return UserOutWithPassword(
            id=id, hashed_password=hashed_password, **old_data
        )

    def record_to_user_in_to_out_without_password(self, id: int, user: UserIn):
        old_data = user.dict()
        return UserOut(id=id, **old_data)

    def record_to_user_out(self, record):
        return UserOut(
            id=record[0],
            first=record[1],
            last=record[2],
            username=record[3],
            email=record[4],
        )

    def record_to_hashed_user_out(self, record):
        return UserOutWithPassword(
            id=record[0],
            first=record[1],
            last=record[2],
            username=record[3],
            email=record[4],
            hashed_password=record[5],
        )

    def record_to_user_with_pw_out(self, record):
        return UserOut(
            id=record[0],
            hashed_password=record[1],
        )
