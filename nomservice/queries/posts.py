from pydantic import BaseModel
from typing import List, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class PostIn(BaseModel):
    user_id: int
    topic: str
    description: str
    posted_on: date


class PostOut(BaseModel):
    id: int
    user_id: int
    topic: str
    description: str
    posted_on: date


class PostRepository:
    def create_post(self, post: PostIn) -> Union[PostOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO posts (
                            user_id, topic, description, posted_on
                        )
                        VALUES (
                            (SELECT id FROM users WHERE id = %s),
                            %s, %s, %s
                        )
                        RETURNING id;
                        """,
                        [
                            post.user_id,
                            post.topic,
                            post.description,
                            post.posted_on,
                        ],
                    )
                    id = result.fetchone()[0]
                    if id is None:
                        return None
                    return self.record_create_post(id, post)
        except Exception:
            return {"message": "Could not sucessfully created a post"}

    def get_all_posts(self) -> Union[List[PostOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, user_id, topic, description, posted_on
                        FROM posts
                        ORDER BY id DESC
                        """
                    )
                    return [self.record_post_in_to_out(record) for record in result]
        except Exception:
            return None

    def get_one_post(self, post_id: int) -> Union[PostOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, user_id, topic, description, posted_on
                        FROM posts
                        WHERE id = %s
                        """,
                        [post_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_post_in_to_out(record)
        except Exception:
            return {"message": "Post was not sucessfully found"}

    def get_all_user_posts(self, user_id: int) -> Union[List[PostOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            user_id,
                            topic,
                            description,
                            posted_on
                        FROM posts
                        WHERE user_id = %s
                        ORDER BY id DESC
                        """,
                        [user_id],
                    )
                    return [self.record_post_in_to_out(record) for record in result]
        except Exception:
            return None

    def update_post(self, post_id: int, post: PostIn) -> Union[PostOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM posts
                        WHERE id = %s
                        """,
                        [post_id],
                    )
                    fetch = result.fetchone()
                    if not fetch:
                        return {"message": "Post does not exist"}
                    db.execute(
                        """
                        UPDATE posts
                        SET user_id = %s,
                            topic = %s,
                            description = %s,
                            posted_on = %s
                        WHERE id = %s
                        """,
                        [
                            post.user_id,
                            post.topic,
                            post.description,
                            post.posted_on,
                            post_id,
                        ],
                    )
                    return self.record_create_post(post_id, post)
        except Exception:
            return {"message": "Updating post failed"}

    def delete_post(self, post_id: int) -> Union[bool, Error]:
        try:
            with pool.connection() as connection:
                with connection.cursor() as db:
                    db.execute(
                        """
                        DELETE from posts
                        WHERE id = %s
                        """,
                        [post_id],
                    )
                    return True
        except Exception:
            return {"message": "Post does not exist"}

    def record_create_post(self, id: int, post: PostIn):
        old_data = post.dict()
        return PostOut(id=id, **old_data)

    def record_post_in_to_out(self, record):
        return PostOut(
            id=record[0],
            user_id=record[1],
            topic=record[2],
            description=record[3],
            posted_on=record[4],
        )
